﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System.Threading;
using System;

public class PlayerDetection : MonoBehaviour
{
    Transform player;
    Transform enemy;
    float Distance_;
    bool playersDetected = false;

    
    // Update is called once per frame
    void Update()
    {
        if (!playersDetected)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

            if (players.Length == 2)
            {
                playersDetected = true;
                player = players[0].transform;
                enemy = players[1].transform;

            }
        }
        else
        {
            Distance_ = Vector3.Distance(player.position, enemy.position);
            Debug.Log(Distance_);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public const float healthPlayer = 100;
    public float currentHealht = healthPlayer;
    public RectTransform healthBar;

    public void TakeDamage(float amount)
    {

        //Checl Health Setup Photon step 5 if something more is needed like photon.view = mine
        currentHealht -= amount;
        if (currentHealht <= 0)
        {
            currentHealht = 0;
            Debug.Log("DEAD");
            GameManager.Instance.Respawn();

        }

        healthBar.sizeDelta = new Vector2(currentHealht, healthBar.sizeDelta.y);
    }
}

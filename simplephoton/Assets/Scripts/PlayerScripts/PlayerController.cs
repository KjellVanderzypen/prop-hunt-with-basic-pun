﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

//jump procedure below 
//https://www.youtube.com/watch?v=vdOFUFMiPDU

public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    [Tooltip("The Player attribute variables")]
    public float speed = 4.0f;
    public float rotationSpeed = 100.0f;
    public float jumpforce = 5;
    public const float healthPlayer = 100;
    public float currentHealht = healthPlayer;
    [Tooltip("The health toolbar")]
    public RectTransform healthBar;

    private Rigidbody rb;

    public Camera player1Cam;
    public Camera player2Cam;

  
    // Start is called before the first frame update


   
    void Start()
    {
       
        /*CameraWork _cameraWork = this.gameObject.GetComponent<CameraWork>();


        if (_cameraWork != null)
        {
            if (photonView.IsMine)
            {
                _cameraWork.OnStartFollowing();
            }
        }
        else
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> CameraWork Component on playerPrefab.", this);
        }
        */

        rb = GetComponent<Rigidbody>();
    }
    void Awake()
    {
        if (photonView.IsMine)
        {
            PlayerController.LocalPlayerInstance = this.gameObject;
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(this.gameObject);
    }
    // Update is called once per frame
    void Update()
    {



        if (photonView.IsMine)
        {
            Transform player1Cam = this.transform.Find("PlayerCamera");
            if (player1Cam)
            {
                player1Cam.gameObject.SetActive(true);

            }
        }
        
       
    
       

        if (photonView.IsMine)
        {
            running();
            GetComponent<MeshRenderer>().material.color = Color.blue;
        }
        else
        {
            GetComponent<MeshRenderer>().material.color = Color.red;
        }
       
    }
    public void running()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);
    }
    public void jump()
    {
        if (Input.GetKeyDown("Space"))
        {
            rb.AddForce(Vector3.up * jumpforce, ForceMode.Impulse);
        }
    }

    public void TakeDamage(float amount)
    {

        //Check Health Setup Photon step 5 if something more is needed like photon.view = mine
        currentHealht -= amount;
        if (currentHealht <= 0)
        {
            currentHealht = 0;
            Debug.Log("DEAD");
            GameManager.Instance.Respawn();

        }

        healthBar.sizeDelta = new Vector2(currentHealht, healthBar.sizeDelta.y);
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(currentHealht);
        }
        else
        {
            // Network player, receive data
            this.currentHealht = (float)stream.ReceiveNext();
        }
    }
}

﻿// MoveTo.cs
using UnityEngine;
using UnityEngine.AI;

public class MoveTo : MonoBehaviour
{

    public Transform goal;
    public Transform agentPos;
    private ArduinoChasing arduino;

    private void Start()
    {
        GameObject arduinoObject = GameObject.Find("ArduinoManager");
        if (arduinoObject != null)
        {
            arduino = arduinoObject.GetComponent<ArduinoChasing>();
        }
    }
    private void Update()
    {
        moveTo();  
    }
    public void moveTo()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();

        //GO TO TARGET
        if (Input.GetKeyUp(KeyCode.X))
        {

            agent.destination = goal.position;
            Debug.Log("x key was pressed");


        }

        if (arduino != null && arduino.xButtonPressed)
        {

            agent.destination = goal.position;

        }

    }
}


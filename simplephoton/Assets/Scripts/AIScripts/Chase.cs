﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chase : MonoBehaviour
{
   
    public Transform target;
    private bool switcher = false;
    private ArduinoChasing arduino;

    private void Start()
    {
        GameObject arduinoObject = GameObject.Find("ArduinoManager");
        if (arduinoObject != null)
        {
            arduino = arduinoObject.GetComponent<ArduinoChasing>();
        }
    }
    // Update is called once per frame
    void Update()
    {
       
        //CHASE TARGET

        Chaser();
    }
    public void Chaser()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();

        if (Input.GetKeyUp(KeyCode.N))
        {
            // flip boolean
            switcher = !switcher;
        }
        if (arduino != null && arduino.nButtonPressed)
        {
            switcher = !switcher;
            arduino.nButtonPressed = false;
        }

        if (switcher)
        {
            agent.destination = agent.nextPosition;
        }
        else
        {
            agent.destination = target.position;
        }
    }
           
   
}



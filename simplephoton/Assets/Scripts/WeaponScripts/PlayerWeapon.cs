﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerWeapon : MonoBehaviourPunCallbacks, IPunObservable 
{


    public GameObject bulletPrefab;
    public Transform gunEnd;

    public float speedBullet = 30;
    public float fireRate = 15;
    public float lifeTimeBullet = 3;


    private float nextTimetoFire = 0;

    bool IsFiring;

    #region IPunObservable implementation


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(IsFiring);
        }
        else
        {
            // Network player, receive data
            this.IsFiring = (bool)stream.ReceiveNext();
        }
    }
    #endregion
    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            ProcessInputs();
        }
        

    }
    void Fire()
    {
        GameObject bullet = Instantiate(bulletPrefab);
        Physics.IgnoreCollision(bullet.GetComponent<Collider>(),
            gunEnd.parent.GetComponent<Collider>());

        bullet.transform.position = gunEnd.position;

        Vector3 rotation = bullet.transform.rotation.eulerAngles;

        bullet.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);

        bullet.GetComponent<Rigidbody>().AddForce(gunEnd.forward * speedBullet, ForceMode.Impulse);

        StartCoroutine(DestroyBulletAfterLifeTime(bullet, lifeTimeBullet));
    }
    void ProcessInputs()
    {
        
        if (Input.GetButtonDown("Fire1") && Time.time >= nextTimetoFire)
        {
            if (!IsFiring)
            {
                nextTimetoFire = Time.time + 1f / fireRate;
                Fire();
                IsFiring = true;
            }
            if (Input.GetButtonUp("Fire1") && Time.time >= nextTimetoFire)
            {
                if (IsFiring)
                {
                    nextTimetoFire = Time.time + 1f / fireRate;
                    Fire();
                    IsFiring = false;
                }
            }
            IsFiring = false;
        }
    }
    //the bullet we instantiated is gonna be destroyed after an amounf of time
    private IEnumerator DestroyBulletAfterLifeTime(GameObject bullet, float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(bullet);
    }
}

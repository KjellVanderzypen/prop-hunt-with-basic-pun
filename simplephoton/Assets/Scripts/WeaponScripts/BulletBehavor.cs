﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavor : MonoBehaviour
{
    public float bulletDamage = 10;

    private void OnTriggerEnter(Collider hit)
    {
        PlayerHealth health = hit.GetComponent<PlayerHealth>();
        if (health != null)
        {
            health.TakeDamage(bulletDamage);
        }
    }
}

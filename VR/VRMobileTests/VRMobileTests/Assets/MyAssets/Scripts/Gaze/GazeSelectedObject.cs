﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeSelectedObject : MonoBehaviour
{
    // Start is called before the first frame update
    public Image UIImageWhereToShow;
    private GameObject attachedObject = null;

    public void Start()
    {
        attachedObject = null;
    }
    public GameObject GetAttachedObject()
    {
        return attachedObject;
    }
    public Sprite GetAttachedSprite()
    {
        return UIImageWhereToShow.sprite;
    }

    public void SetAttachedObject(GameObject selectedObject)
    {
        attachedObject = selectedObject;
        SetSprite(selectedObject.GetComponentInChildren<InventoryItem>().sprite);
        attachedObject.SetActive(false);
    }

    private void SetSprite(Sprite s)
    {
        UIImageWhereToShow.enabled = true;
        UIImageWhereToShow.sprite = s;
        
    }
    public void Clear()
    {
        attachedObject = null;
        UIImageWhereToShow.sprite = null;
        UIImageWhereToShow.enabled = false;
       // attachedObject.SetActive(true);
    }
}

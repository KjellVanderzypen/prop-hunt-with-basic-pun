﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GazeTimer : MonoBehaviour
{
    public Image imgGazeCursor;
    public float timeToGaze = 3;
    private float gazeTime;
    public bool isGazing = false;

    public int distanceOfRay = 20;
    private RaycastHit hit;
    // Start is called before the first frame update
    void Start()
    {
        isGazing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(isGazing)
        {
            gazeTime += Time.deltaTime;
            imgGazeCursor.fillAmount = gazeTime / timeToGaze;

            if(imgGazeCursor.fillAmount == 1)
            {
                Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                if (Physics.Raycast(ray, out hit, distanceOfRay))
                {
                    if(hit.transform.CompareTag("TeleportLocation"))
                    {
                        hit.transform.GetComponent<TeleportPlayer>().Teleport();
                        GazOff();
                    }
                }
            }
        }

       
    }

    public void GazeOn()
    {
        isGazing = true;
    }

    public void GazOff()
    {
        isGazing = false;
        gazeTime = 0;
        imgGazeCursor.fillAmount = 0;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : MonoBehaviour
{
    public Sprite sprite;
    private GazeSelectedObject selectedObject = null;
    // Start is called before the first frame update
    private void Start()
    {
        selectedObject = (GazeSelectedObject)GameObject.FindGameObjectWithTag("SelectedGazeTargetImage").GetComponent<GazeSelectedObject>();
    }

    public void IsGazeSelected()
    {
        if(selectedObject)
        {
            selectedObject.SetAttachedObject(this.gameObject);
            AudioManager.PlayClipOnce(AudioManager.AudioID.PICKUPITEM);
        }
        
    }
}

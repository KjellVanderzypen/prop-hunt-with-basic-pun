﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryFollowCamBottom : MonoBehaviour
{
    public float angleThreshHold;
    public GameObject objectToShowBelowThreshold;
    public Camera camera;
    private float stableTime;
    private float timeSinceLastDown;
    private bool isShowing;
    private Transform originalParent;
    private Vector3 originalLocalPos;
    private Quaternion originalLocalRot;
    public GameObject dummyParent;

    private float sinAngleThreshHold ;
    // Start is called before the first frame update
    void Start()
    {
        angleThreshHold = angleThreshHold *Mathf.Deg2Rad;
        sinAngleThreshHold = -Mathf.Sin(angleThreshHold);
        timeSinceLastDown = 999999;
        stableTime = 1f;
        isShowing = false;
        originalParent = objectToShowBelowThreshold.transform.parent;
        originalLocalPos = objectToShowBelowThreshold.transform.localPosition;
        originalLocalRot = objectToShowBelowThreshold.transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
       
        //check if visible and show hide
        if (camera.transform.forward.y < sinAngleThreshHold )
        {
            timeSinceLastDown = 0;
        }
        else
        {
            timeSinceLastDown += Time.deltaTime;
        }

        if ( timeSinceLastDown <= stableTime && !isShowing)
        {
            //startShowing
            objectToShowBelowThreshold.GetComponent<Canvas>().enabled = true;
            objectToShowBelowThreshold.GetComponent<CanvasScaler>().enabled = true;
            objectToShowBelowThreshold.GetComponent<GraphicRaycaster>().enabled = true;
            isShowing = true;
            //stop following camera;
            dummyParent.transform.position = camera.transform.position;
            dummyParent.transform.rotation = camera.transform.rotation;
            objectToShowBelowThreshold.transform.SetParent(dummyParent.transform);
        }
        else if (isShowing && timeSinceLastDown > stableTime)
        {
            //stop Showing
            objectToShowBelowThreshold.GetComponent<Canvas>().enabled = false;
            objectToShowBelowThreshold.GetComponent<CanvasScaler>().enabled = false;
            objectToShowBelowThreshold.GetComponent<GraphicRaycaster>().enabled = false;
            isShowing = false;
            objectToShowBelowThreshold.transform.SetParent( originalParent);
            objectToShowBelowThreshold.transform.localPosition = originalLocalPos;
            objectToShowBelowThreshold.transform.localRotation = originalLocalRot;
        }

        
    }
}

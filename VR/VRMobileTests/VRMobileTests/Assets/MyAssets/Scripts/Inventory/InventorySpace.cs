﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySpace : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject storedObject;
    public Image imageToShow;
    private GazeSelectedObject gazeSelectedObjectScript = null;
    private bool isTaken = false;
    private Sprite originalSprite;

    void Start()
    {
        gazeSelectedObjectScript = (GazeSelectedObject)GameObject.FindGameObjectWithTag("SelectedGazeTargetImage").GetComponent<GazeSelectedObject>();
        originalSprite = imageToShow.sprite;
        isTaken = false;
    }


    public void SetObject()
    {
          
        if (!isTaken)//handle empty gaze at
        {
            storedObject = gazeSelectedObjectScript.GetAttachedObject();
            if (storedObject)
            {
                imageToShow.sprite = gazeSelectedObjectScript.GetAttachedSprite();
                gazeSelectedObjectScript.Clear();
                isTaken = true;
                AudioManager.PlayClipOnce(AudioManager.AudioID.STOREITEM);
            }
        }
        else
        {          
            if (!gazeSelectedObjectScript.GetAttachedObject()) //HANDLE TAKING THINGS OUT AGAIN WHEN GAZE OBJECT IS EMPTY
            {
                storedObject.SetActive(true);
                gazeSelectedObjectScript.SetAttachedObject(storedObject);
                Clear();
                isTaken = false;
                AudioManager.PlayClipOnce(AudioManager.AudioID.PICKUPITEM);
            }
            else//TODO HANDLE COMBINING OBJECTS
            {

            }
        }
    }

    public void Clear()
    {
        storedObject = null;
        imageToShow.sprite = originalSprite;
        isTaken = false;
    }
}

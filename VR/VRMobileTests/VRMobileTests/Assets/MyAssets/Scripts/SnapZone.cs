﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapZone : MonoBehaviour
{
    // Start is called before the first frame update
    
    private GazeSelectedObject gazeSelectedObjectScript = null;
    private bool isTaken = false;
    private Transform snapLocation;
    GameObject storedObject = null;
    void Start()
    {
        gazeSelectedObjectScript = (GazeSelectedObject)GameObject.FindGameObjectWithTag("SelectedGazeTargetImage").GetComponent<GazeSelectedObject>();
        snapLocation = this.transform.GetChild(0);
        isTaken = false;
    }


    public void SetObject()
    {
        if (!isTaken)//handle empty gaze at
        {
            storedObject = gazeSelectedObjectScript.GetAttachedObject();
            if (storedObject)
            {
                storedObject.SetActive(true);
                storedObject.transform.position = snapLocation.transform.position;
                storedObject.transform.rotation = snapLocation.transform.rotation;
                gazeSelectedObjectScript.Clear();
                isTaken = true;
                AudioManager.PlayClipOnce(AudioManager.AudioID.STOREITEM);
            }
        }
        else
        {
            if (!gazeSelectedObjectScript.GetAttachedObject()) //HANDLE TAKING THINGS OUT AGAIN WHEN GAZE OBJECT IS EMPTY
            {
                storedObject.SetActive(true);
                gazeSelectedObjectScript.SetAttachedObject(storedObject);
                Clear();
                isTaken = false;
                AudioManager.PlayClipOnce(AudioManager.AudioID.PICKUPITEM);
            }
            else//TODO HANDLE COMBINING OBJECTS
            {

            }
        }
    }

    public void Clear()
    {
        storedObject = null;
        isTaken = false;
    }
}

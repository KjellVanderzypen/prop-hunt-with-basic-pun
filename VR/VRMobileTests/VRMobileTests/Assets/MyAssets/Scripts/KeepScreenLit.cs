﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepScreenLit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Disable screen dimming
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Screen.brightness = 1.0f;
    }

    private void Update()
    {
        
    }


}

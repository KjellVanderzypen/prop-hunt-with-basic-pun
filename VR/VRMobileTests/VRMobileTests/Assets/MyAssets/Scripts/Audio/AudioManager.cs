﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public enum AudioID
    {
        FOOTSTEPS,
        PICKUPITEM,
        STOREITEM,
    }

    public AudioSource[] setPredefClips;
    private static AudioSource[] predefinedClips;

    // Start is called before the first frame update
    void Start()
    {
        predefinedClips = setPredefClips;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlayClipOnce(AudioSource audioS)
    {
        audioS.Play();
    }

    public static void StartClipLoop(AudioSource audioS)
    {
        audioS.loop = true;
        audioS.Play();
    }
    public static void StopClipLoop(AudioSource audioS)
    {
        audioS.Stop();
    }
    public static void PlayClipOnce(AudioID id)
    {
        predefinedClips[(int)id].Play();
    }

    public static void StartClipLoop(AudioID id)
    {
        predefinedClips[(int)id].loop = true;
        predefinedClips[(int)id].Play();
    }
    public static void StopClipLoop(AudioID id)
    {
        predefinedClips[(int)id].Stop();
    }

   
}

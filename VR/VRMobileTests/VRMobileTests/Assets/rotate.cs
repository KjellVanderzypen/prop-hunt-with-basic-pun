﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{
    public float spinSpeed;
    // Start is called before the first frame update
    void Start()
    {
        spinSpeed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, spinSpeed * Time.deltaTime, 0);
    }

    public void ChangeSpin()
    {
        
        spinSpeed = 20f; 
    }

    public void StopSpin()
    {

        spinSpeed = 0f;
    }
}
